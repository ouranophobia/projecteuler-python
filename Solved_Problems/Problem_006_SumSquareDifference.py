sumOfSquares, squareOfSums = 0, 0

for i in range(1, 101): sumOfSquares, squareOfSums = sumOfSquares + i * i, squareOfSums+i

print((squareOfSums * squareOfSums) - sumOfSquares)