import math


# Returns TRUE if n is prime
def isprime(n):
    if n == 1: return False
    if n == 2: return True
    if n % 2 == 0: return False
    for i in range(3, n, 2):
        if n % i == 0: return False
        return True

# Euclid's formula for a Pythagorean triple
def pythagoreanTriple(n, m):
    triple = [m * m - n * n, 2 * m * n, m * m + n * n]

    return triple


# Returns number of divisors of n
def numOfDivisors(n):
    count = 0
    for i in range(1, int(math.sqrt(n))):
        if n == 1:
            count = 1
            break
        if n % i == 0:
            count += 2

    return count - 1


def sieveprimes(n):
    # Create a boolean array "prime[0..n]" and initialize
    #  all entries it as true. A value in prime[i] will
    # finally be false if i is Not a prime, else true.
    prime = [True for i in range(n + 1)]
    p = 2
    while p * p <= n:
        # If prime[p] is not changed, then it is a prime
        if prime[p]:

            # Update all multiples of p
            for i in range(p * 2, n + 1, p):
                prime[i] = False
        p += 1


# Returns the set of proper divisors of n
def setOfDivisors(n):
    divisors = []
    for i in range(2, int((n / 2)) + 1):
        if n == 1:
            break
        if n % i == 0:
            divisors.append(int(n / i))
    divisors.append(1)
    return divisors


# Returns true if n is a perfect number
def isPerfectNum(n):
    arr = setOfDivisors(n)
    sum = 0
    for i in arr:
        sum += i
    return sum == n


# Returns true if n is an abundant number
def isAbundantNum(n):
    arr = setOfDivisors(n)
    sum = 0
    for i in arr:
        sum += i
    return sum > n


# Returns the sum of all elements in an array
def sumOfElements(n):
    sum = 0
    for i in n:
        sum += i

    return sum


# Returns array of alphabet numbers of string s
def getABC(s):
    temp = s.upper()
    abc = [];
    A = ord('A')
    for c in temp:
        if 'A' <= c <= 'Z':
            abc.append(ord(c) - A + 1)

    return abc
